# tp3_installer

Install typo3 
- develop
- stage
- production

This is an installer for typo3 dev environment with unit, functional and acceptance  testing and developer tools
```bash
 composer --dev --stability=dev create-project web-tp3/tp3_installer:dev-master
```

You can pick a Version/Branch of typo3 within the the Installer 
dev-master (9.x-dev) 
dev-8.x-dev (8.x-dev)
Dependiencies will be maintained within the different Versions. So think about disconnecting your git.

###prerequisites

php 7.x with the extensions:

	"ext-soap": "*",
    "ext-gd": "*",
    "ext-fileinfo": "*",
    "ext-zlib": "*",
    "ext-openssl": "*",
    "ext-zip": "*",
    "ext-mysqli": "*",
    
and webserver apache/nginx and a database Mysql/MariaDB.

###install

Simply installs typo3 out of the composer.json for typo3 LTS 9.x -> dev-master 
Helps you pick the Versions for testing and stable landscapes. 
 
 Feel free to edit the piplines for automated deployment
 For the first test we suite this package with a docker image running with all needed php features to get started with testing.
 
 Combined you can make automated tests before deployment.
 
 https://bitbucket.org/web-tp3/docker
 